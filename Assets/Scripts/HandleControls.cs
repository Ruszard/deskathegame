namespace VRTK
{
	using UnityEngine;
	using TMPro;

	public class HandleControls : MonoBehaviour
	{
		public TMP_Text text;
		public GameObject controller;

		public MeshGenerator meshGenerator;

		private Vector3 plankStart;
		private Vector3 plankEnd;

		private void Start()
		{
			plankStart = Vector3.zero;
			plankEnd = Vector3.zero;

			if (GetComponent<VRTK_ControllerEvents>() == null)
			{
				VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTK_ControllerEvents_ListenerExample", "VRTK_ControllerEvents", "the same"));
				return;
			}

			//Setup controller event listeners
			GetComponent<VRTK_ControllerEvents>().TriggerPressed += new ControllerInteractionEventHandler(DoTriggerPressed);
			GetComponent<VRTK_ControllerEvents>().GripPressed += new ControllerInteractionEventHandler(DoGripPressed);
		}

		private void DoTriggerPressed(object sender, ControllerInteractionEventArgs e)
		{
			if (plankStart != Vector3.zero && plankEnd != Vector3.zero)
				return;

			if (plankStart == Vector3.zero)
				plankStart = controller.transform.position;
			else if (plankEnd == Vector3.zero)
				plankEnd = controller.transform.position;	

			if (plankStart != Vector3.zero && plankEnd != Vector3.zero)
				CreateCube ();

			UpdateText ();
		}

		private void DoGripPressed(object sender, ControllerInteractionEventArgs e)
		{
			plankStart = Vector3.zero;
			plankEnd = Vector3.zero;

			UpdateText ();
		}

		private void CreateCube()
		{
			meshGenerator.CreateCube (plankStart, plankEnd);
		}
			
		private void UpdateText()
		{
			text.text = "Start: " + (plankStart == Vector3.zero ? "-" : "+") + "\nEnd: " + (plankEnd == Vector3.zero ? "-" : "+");
		}
	}
}
