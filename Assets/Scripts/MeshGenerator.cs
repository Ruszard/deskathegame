using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof (MeshFilter))]
[RequireComponent(typeof (MeshRenderer))]
public class MeshGenerator : MonoBehaviour {

	private float plankWidth = 0.2f;

	public void CreateCube (Vector3 startPos, Vector3 endPos) {
		Vector3[] vertices = {
			new Vector3 (startPos.x - plankWidth / 2, 0, startPos.z),
			new Vector3 (startPos.x + plankWidth / 2, 0, startPos.z),
			new Vector3 (startPos.x + plankWidth / 2, 0.1f, startPos.z),
			new Vector3 (startPos.x - plankWidth / 2, 0.1f, startPos.z),
			new Vector3 (startPos.x - plankWidth / 2, 0.1f, startPos.z + Vector3.Distance(startPos, endPos)),
			new Vector3 (startPos.x + plankWidth / 2, 0.1f, startPos.z + Vector3.Distance(startPos, endPos)),
			new Vector3 (startPos.x + plankWidth / 2, 0, startPos.z + Vector3.Distance(startPos, endPos)),
			new Vector3 (startPos.x - plankWidth / 2, 0, startPos.z + Vector3.Distance(startPos, endPos)),
		};

		int[] triangles = {
			0, 2, 1, //face front
			0, 3, 2,
			2, 3, 4, //face top
			2, 4, 5,
			1, 2, 5, //face right
			1, 5, 6,
			0, 7, 4, //face left
			0, 4, 3,
			5, 4, 7, //face back
			5, 7, 6,
			0, 6, 7, //face bottom
			0, 1, 6
		};

		Mesh mesh = GetComponent<MeshFilter> ().mesh;
		mesh.Clear ();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		;
		mesh.RecalculateNormals ();
	}
}
